const express = require("express")
const bodyParser = require("body-parser")
const app = express()
const port = 3000
const db = require("./query")
const jwt = require("jwt-simple")
const passport = require("passport")

const multer = require("multer")

var date = new Date().getDate() //Current Date
var month = new Date().getMonth() + 1 //Current Month
var year = new Date().getFullYear() //Current Year
const Storage = multer.diskStorage({
	//image filename
	destination(req, file, callback) {
		callback(null, "./public/images")
	},
	filename(req, file, callback) {
		callback(null, `${date}-${month}-${year}_${userids}_${file.originalname}`)
	}
})
app.use("/images", express.static(__dirname + "/public/images"))
console.log("PATH => ", __dirname + "/images/")

app.use(bodyParser.json())
app.use(
	bodyParser.urlencoded({
		extended: true
	})
)
//////////////////////////  AUTH MIDDLEWARE  /////////////////////////////////////
const auth = async (req, res, next) => {
	console.log(req.headers)
	const token = req.header("authorization")
	if (!token) {
		res.status(400).json({ message: "error auth" })
	}
	try {
		const result = await jwt.decode(token, SECRET)
		console.log(result)
		req.email = result.sub
		next()
	} catch (error) {
		res.status(400).json({ message: " token error" })
	}
}
/////////////////////////////////////////////////////////////////////////////////
passport.use(db.jwtAuth)
const SECRET = "my secret key"

app.post("/login", db.loginUser, (req, res) => {
	//create JWT
	const payload = {
		sub: req.body.email,
		iat: new Date().getTime()
	}
	const JWT = jwt.encode(payload, SECRET)
	console.log("user ID = = =>", userids)

	console.log("Payload:", payload)
	console.log("JWT:", JWT)
	res.setHeader("authorization", JWT)
	res.status(200)
	res.send({ token: JWT })
})
app.get("/cardlist", auth, db.cardlist)
app.post("/users", db.createUser)
app.get("/userinfo", auth, db.userHis)
const upload = multer({ storage: Storage }) //destination and file name
app.post("/imgs", auth, upload.array("photo", 2), db.uploadimg) //uploadimage
app.post("/forms", auth, db.createForm) //formsub

app.listen(port, () => {
	console.log(`App running on port ${port}.`)
})
