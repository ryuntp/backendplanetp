const Pool = require("pg").Pool
const pool = new Pool({
	user: "postgres",
	host: "localhost",
	database: "planetP",
	password: "1q2w3e4r",
	port: 5432
})
const bcrypt = require("bcrypt")
const SECRET = "my secret key"
const passport = require("passport")

////////////////////////////////////////// SIGN UP //////////////////////////////////////////////////
const createUser = (request, response) => {
	// To create a new user
	const { name, email, password, mobile } = request.body
	pool.query(
		"SELECT EXISTS(SELECT 1 FROM userss WHERE email=$1)", //Check if the email is already in the database
		[email],
		(error, results) => {
			console.log("email is already exist?", results.rows[0].exists)
			if (results) {
				if (results.rows[0].exists === true) {
					// if the email exists in the database ◥◤
					console.log("email existtttttt")
					console.log(
						"This is response ======>",
						response.status(409).statusCode
					)
					response.status(409).json(name)
				} else {
					// if the email doesn't exist in the database ◥◤
					console.log("Body => ", request.body)
					hashpwd = bcrypt.hash(password, 10, (err, hash) => {
						console.log("HashPwd => ", hash)
						pool.query(
							"INSERT INTO userss (name,email,password,mobile) VALUES ($1,$2,$3,$4)",
							[name, email, hash, mobile],
							(error, results) => {
								console.log("errorrrr", error)
								console.log("results:", results)
								console.log([name, email, hash, mobile])
								if (error) {
									response.status(500).json(error)
								}
								response.status(201).json(name)
							}
						)
					})
				}
			}
		}
	)
}

//////////////////////////////////////// JWT & LOGIN //////////////////////////////////////////////////////
//decode jwt
const ExtractJwt = require("passport-jwt").ExtractJwt
//declare strategy
const JwtStrategy = require("passport-jwt").Strategy

const jwtOptions = {
	jwtFromRequest: ExtractJwt.fromHeader("authorization"),
	secretOrKey: SECRET
}

const callback = async (a, b) => {
	console.log("payload", a)
	const email = a.sub
	let x = await pool.query("SELECT email FROM userss WHERE email = $1", [email])
	console.log(x.rows[0].email)

	if (a.sub === x.rows[0].email) b(null, true)
	else b(null, false)
}

const jwtAuth = new JwtStrategy(jwtOptions, callback)
const requireJWTAuth = passport.authenticate("jwt", { session: false }) //declare

const loginUser = (request, response, next) => {
	// To login user
	const { email, password } = request.body
	pool.query(
		// Check if the email and password from inputs match the one in the database
		"SELECT email,password,userid FROM userss WHERE email = $1",
		[email],
		(error, results) => {
			console.log("Results => ", results)
			if (results.rowCount > 0) {
				bcrypt.compare(password, results.rows[0].password, (err, res) => {
					if (res) {
						userids = results.rows[0].userid //define userids as an user id

						next()
					} else {
						// if not match
						response.status(400).end()
					}
				})
			} else {
				response.status(401).end()
			}
		}
	)
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////

const uploadimg = (request, response) => {
	const { latitude, longitude } = request.body
	const files = request.files
	console.log("file = = => ", request.files)
	console.log("body = = =>>>", request.body)
	var date = new Date().getDate() //Current Date
	var month = new Date().getMonth() + 1 //Current Month
	var year = new Date().getFullYear() //Current Year
	const URI = `http://172.30.53.67:3000/images/${date}-${month}-${year}_${userids}_${
		files[0].originalname
	}`
	pool.query(
		"INSERT INTO uploadimage(lat,long,url,userid,curtime) VALUES ($1,$2,$3,$4,CURRENT_TIMESTAMP)",
		[latitude, longitude, URI, userids],
		(error, results) => {
			console.log("results:", results)
			console.log([latitude, longitude])
			if (error) {
				response.status(500).json(error)
			}
			response.status(201).json("eiei")
		}
	)
}

const createForm = (request, response) => {
	// To create a form
	const { name, mobile, email, gender, message } = request.body

	pool.query(
		"INSERT INTO formsubmission(name,mobile,email,gender,message,userid) VALUES ($1,$2,$3,$4,$5,$6)",
		[name, mobile, email, gender, message, userids],
		(error, results) => {
			console.log("results:", results)
			console.log([name, mobile, email, gender, message, userids])
			if (error) {
				response.status(500).json(error)
			}
			response.status(201).json(name)
		}
	)
}

const userHis = (request, response) => {
	// To display user profile at the user profile screen ◥◤
	const email = request.email

	pool.query(
		"SELECT * FROM userss WHERE email = $1",
		[email],
		(error, results) => {
			if (error) {
				throw error
			}
			response.status(200).json(results.rows)
		}
	)
}

const cardlist = (request, response) => {
	pool.query(
		"SELECT * FROM uploadimage ORDER BY imgid DESC",
		(error, results) => {
			if (error) {
				throw error
			}
			response.status(200).json(results.rows)
		}
	)
}
module.exports = {
	createUser,
	loginUser,
	requireJWTAuth,
	jwtAuth,
	uploadimg,
	createForm,
	userHis,
	cardlist
}
